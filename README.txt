This zip file contains:
=======================

elastic_settings.txt           - open this file and cut/paste the contents into the console in User guide Step 3

create_index.sh                - a linux shell script to create an index named nipper with the correct mappings

delete_index.sh                - a linux shell script to delete an index named nipper.

nipper_kibana_dashboard.ndjson - import this file in Step 5 when importing objects. This dashboard is looking for indexes matching nipper**

l.conf                         - this is the file used as a parameter to logstash to connect to your elastic instance. This example has no credentials and points to localhost.

ls_with_creds.conf             - this file is an example of connecting logstash to an online example of elasticsearch - the url / username/password are examples only, you need to substitute them with your own instance values.

start_elk.ps1                  - this is a powershell script that helps to run up Elastic and Kibana within Docker on the local machine. The script must be run from a powershell prompt with administrator privilages

geodata.json                   -  an example file that ties hostanmes to geo locations

mapit.py                       - a python file that enriches the nipper json, python mapit.py -f siems.json   has output siems_out.json on ndjson format, siems_out_formatted.json in human readable json, and ./events directory contains individual files for each event ( only created for debug purposes )

dashboard_index_id.txt         - when creating the elastic index pattern this uuid matches the one used in the ndjson dashboard files. Use the advanced settings button to enter it.

 
Before the Docker Containers can be run up, Docker Desktop ( https://www.docker.com/products/docker-desktop ) must be installed.


Note:

When running logstash as

cat nipper.json | logstash -f l.conf

In some installations logstash fails with permission errors.  You can overcome this by supplying command line direction to say where to write log output.

mkdir ./ls_log
cat nipper.json | logstash -f l.conf --path.data ./ls_log -l ./ls_log
