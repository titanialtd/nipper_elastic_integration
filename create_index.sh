curl -X PUT "localhost:9200/nipper?pretty" -H 'Content-Type: application/json' -d'
{
       "settings": {
        "index": {
            "number_of_shards": 1,
            "number_of_replicas": 0,
            "mapping.depth.limit": 500
        }
    },
    "mappings": {
        "properties": {
		"location" : {
		                  "type" : "geo_point"
			},
            "date_time": {
                "type": "date",
                "format": "EEE MMM d[d] HH:mm:ss yyyy"
            },
            "epoch": {
                "type": "date"
            },
            "summary": {
                "type": "text",
                "fields": {
                    "keyword": {
                        "type": "keyword",
                        "ignore_above": 8192
                    }
                }
            },
            "check": {
                "type": "text",
                "fields": {
                    "keyword": {
                        "type": "keyword",
                        "ignore_above": 8192
                    }
                }
            },
            "fix": {
                "type": "text",
                "fields": {
                    "keyword": {
                        "type": "keyword",
                        "ignore_above": 8192
                    }
                }
            },
            "description": {
                "type": "text",
                "fields": {
                    "keyword": {
                        "type": "keyword",
                        "ignore_above": 8192
                    }
                }
            },
            "impact": {
                "properties": {
                    "description": {
                        "type": "text",
                        "fields": {
                            "keyword": {
                                "type": "keyword",
                                "ignore_above": 8192
                            }
                        }
                    }
                }
            },
            "reccomendation": {
                "properties": {
                    "description": {
                        "type": "text",
                        "fields": {
                            "keyword": {
                                "type": "keyword",
                                "ignore_above": 8192
                            }
                        }
                    }
                }
            },
            "ease": {
                "properties": {
                    "description": {
                        "type": "text",
                        "fields": {
                            "keyword": {
                                "type": "keyword",
                                "ignore_above": 8192
                            }
                        }
                    }
                }
            }
        }
    }
}'
