# This function call docker version, which will not succeeed until Docker Desktop is fully initialised
function WaitForDocker {
	Do {
		$the_time = Get-Date -DisplayHint Time
		Write-Host "Waiting for Docker Desktop to initialise : ", $the_time
		Start-Sleep -s 2
		$answer = docker ps 2>&1>$null
	} while ( $? -eq $False )
	Write-Host "Docker Desktop up and running"
}

# Look to see if Docker Desktop is running
$ProcessActive = Get-Process "Docker Desktop" -ErrorAction SilentlyContinue
if ( $ProcessActive -eq $null )
{
	# Docker desktop is not running, so start it , ad wait until it is properly initialised
	Write-Host "Start Docker Desktop"
	& 'C:\Program Files\Docker\Docker\Docker Desktop.exe' 
	Write-Host "Now wait for Docker Desktop to initialise, this takes around 1 minute on WSL 1, and around 10 seconds on WSL 2"
	$start_time = Get-Date
	Start-Sleep -s 5
	WaitForDocker
	$end_time = Get-Date
	$diff = New-TimeSpan -Start $start_time -End $end_time 
	Write-Host "Starting Docker Desktop took ", $diff.totalseconds , " seconds"
}
else
{
	Write-Host "Docker Desktop is already running - just start the containers"
}
#  Stop and remove any previously run up container instances
$kibana_instance  = docker ps -a -q  -f "name=kibana"
$elastic_instance = docker ps -a -q  -f "name=myelastic"
if ( $null -ne $kibana_instance  ){
	Write-Host "Stop and remove old Kibana container"
	docker stop   $kibana_instance
	docker rm   $kibana_instance
}
if ( $null -ne $elastic_instance  ){
	Write-Host "Stop and remove old Elastic container"
	docker stop   $elastic_instance
	docker rm   $elastic_instance
}
# Now run up Elastci and Kibana containers , and link them together
[string]$dver="7.8.0"
echo ${dver}
docker run -d --name myelastic -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" docker.elastic.co/elasticsearch/elasticsearch:${dver}
Write-Host "Started Elastic "
docker run -d --name kibana --link myelastic:elasticsearch  -p 5601:5601 docker.elastic.co/kibana/kibana:${dver}
Write-Host "Started Kibana "
docker ps
