import requests,os,json,time,uuid,copy
import argparse
from datetime import datetime
from datetime import timedelta
from dateutil.parser import parse

parser = argparse.ArgumentParser()
parser.add_argument("-f","--file",default="log1.json")
parser.add_argument("-p","--prefix",default="./events/")
parser.add_argument("-r","--repeat",type=int,default=0)
cla = parser.parse_args()
print(cla)
json_file = "nipper.json"
if cla.file is not None:
    json_file = cla.file

directory='.'
NDJSON=1
NORMAL=2
filetype=NDJSON

#JSON can be in an array, or newline delimited. Test to see which
with open(json_file) as fn:
    first = fn.readline()
    if first[0] == '[':
        filetype = NORMAL
    fn.close()
#
#  Read in the JSON from Nipper.  
#
print("open {} ".format(json_file))
js=[]
with open(json_file) as fn:
    if filetype == NORMAL:
        js=json.load(fn)
    else:
        for line in fn:
            json_object = json.loads(line)
            js = js + [json_object]
print("read in {} json file ok with {} objects\n".format(filetype,len(js)))

#delete a filename - checking that it exists first 
def delete_it(filename):
    if os.path.exists(filename):    
        os.remove(filename)

#read geodata we created in geodata.json - edit that file to match your own hostname / geo positioning
def read_geodata(geod):
        with open('geodata.json',"r") as geo:
                for line in geo:
                        jo = json.loads(line)
                        hn = jo['device']['hostname']
                        loc = jo['location']
                        geod[hn] = [loc,jo['name']]

geo_data = {}
read_geodata(geo_data)
print("read {} geodata device locations".format(len(geo_data)))

prefix  = "./events/"
if cla.file is not None:
    prefix = cla.prefix

head, tail =  os.path.split(json_file)

name, ext = os.path.splitext(tail)

outfile = name + "_out.json"
formatted_outfile = name + "_out_formatted.json"

print("one enriched ndjson file will be written to {}".format(formatted_outfile))
print("one output  file will be written to {}".format(formatted_outfile))
print("individual findings files will be written to the ./events directory".format(formatted_outfile))

delete_it(outfile)
delete_it(formatted_outfile)
repetition = cla.repeat

def write_ndjson_files(events,seq):
  outfile = name +  "_out.json"
  delete_it(outfile)
  with open(outfile,"w") as wf:
    for f in events:
        obj = json.dumps(f)
        wf.write("{}\n".format(obj))
        seq = seq + 1
        fn = "{}evt_{}.json".format(prefix,seq)
        fp=open(fn,"w")
        fp.write("{}\n".format(obj))
        fp.close()
    wf.close()
    return seq

#a UUID to uniquly identify this run
session_uuid = str(uuid.uuid4())
seconds = int(time.time() )*1000
t1 = datetime.now()
seq=0
nipper_id=1

# This function is used to fix some bugs in theoutput of Nipper, where it generates inconsistend types for attribute data.
# Finally , it converts the event to an ECS compliant form,  insertging some core ECS fields, and placing our data into a titania {} dict.
def convert_to_ecs(report):
        # some findings attributes have null attribute name elements, which we suvstitute with nv or x1, and some subelements are just strings when they should be dictionatry types
        if 'findings' in report:
             device = report['findings']
             for finding in device:
                 value =  device[finding]
                 for item in value:
                    value1 = value[item]
                    # if type is a string, encapsulate in a dict with a nominal attribute name of nv
                    if type(value1) == str:
                        value[item] = { "nv" : value1 }
                    else:
                        for i in value1:
                            if i == '':
                                value2 = value1[i]
                                value1.pop(i)
                                value1['x1'] = value2
        #fix  these attributes where some records have an empy string rather than an empty array
        for attr in ['advisories','references']:
            if attr in report:
                #print("found {}".format(attr))
                if report[attr][0] == "":
                    report[attr] = []

        #make sure count is a string type
        if 'count' in report:
             c = report['count']
             report['count'] = str(c)

        nipper_id = report['nipper_id']
        try:
            the_name=''
            if 'device' in report:
                the_name = report['device']['hostname']
            if 'host' in report:
                del report['host']
            
            
            keys = list(report.keys())
            titania = {}
            
            for key in keys:
                #print("item {} value {}".format(key,json.dumps(report[key])))
                titania[key] = report[key]
                del report[key]
            
            report['titania'] = titania
            report['ecs'] = { 'version' : "1.6.0" }
            report['timestamp'] = parse(titania['date_time']).isoformat()
            report['host'] = { 
                               'architecture' : 'x86_64', 
                               'hostname' : the_name ,
                               'id' : the_name , 
                               'ip' : [ titania['device']['collection_ip']  ],
                               'mac' : [] , 
                               'name' : the_name , 
                               'type' : titania['device']['model'] 
                            } 
            report['event'] = { 'module' : "Nipper" , "action" : titania['audit_type'], 'category' : 'network' , 'type' : 'info'}
            report['user']  = { 'name' : 'operator1' }
            report['source'] = { 'ip' : titania['device']['collection_ip'] }

        except Exception as e:
            print("exception in ecs formatting {} event_id {}".format(e,nipper_id))


#for every report in the original add the UUID and some text,  and , based on hostname enrich with geo data
for report in js:
        report['nipper_session'] = session_uuid
        report['nipper_text'] = "Research Network last audit"
        report['date_time'] = t1.strftime("%a %b %d %H:%M:%S %Y")
        report['epoch'] = seconds
        report['nipper_id'] = nipper_id
        nipper_id = nipper_id + 1
        try:
                hn = report['device']['hostname']
                locn = geo_data[hn][0]
                city= geo_data[hn][1]
                report['location'] = locn
                report['city'] = city
                report['device'].pop('not_operating_system',None)
        except Exception as e: 
               pass 
        if 'devices' not in report:
            convert_to_ecs(report)

#write out formatted json for ease of use
with open(formatted_outfile,"w") as wf:
    wf.write(json.dumps(js,indent=4))

# write out files , one per event into a directory, we also allow skipping of a finding, just for test data
# and also a single ndjson file with all records. Splunk accepts either option.
write_ndjson_files(js,seq)

